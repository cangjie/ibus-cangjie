<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright (c) 2012-2024 - The IBus Cangjie authors

    This file is part of ibus-cangjie, the IBus Cangjie input method engine.

    ibus-cangjie is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ibus-cangjie is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ibus-cangjie.  If not, see <http://www.gnu.org/licenses/>.
-->
<interface>
  <requires lib="gtk" version="4.6"/>
  <object class="GtkApplicationWindow" id="setup_dialog">
    <property name="resizable">0</property>
    <property name="accessible-role">dialog</property>
    <property name="icon-name">applications-system</property>
    <property name="modal">True</property>
    <property name="show-menubar">True</property>
    <property name="titlebar">
      <object class="GtkHeaderBar" id="header">
        <child type="start">
          <object class="GtkButton" id="about-button">
            <property name="icon-name">help-about</property>
            <property
              name="tooltip-text"
              translatable="yes"
            >About IBus Cangjie</property>
          </object>
        </child>
      </object>
    </property>
    <child>
      <object class="GtkBox">
        <property name="orientation">vertical</property>
        <property name="margin-top">20</property>
        <property name="margin-bottom">20</property>
        <property name="margin-start">20</property>
        <property name="margin-end">20</property>
        <property name="spacing">10</property>
        <child>
          <object class="GtkBox">
            <property
              name="tooltip-text"
              translatable="yes"
            >We support Cangjie version 3 and 5. Version 3 is commonly used in old Windows system. Version 5 is supported by macOS and newer Windows systems.</property>
            <child>
              <object class="GtkLabel">
                <property name="margin-end">20</property>
                <property
                  name="label"
                  translatable="yes"
                  comments="We support versions 3 and 5 of Cangjie, this option lets users choose the one they prefer using."
                >Input Method Version</property>
              </object>
            </child>
            <child>
              <object class="GtkDropDown" id="version">
                <property name="model">
                  <object class="GtkStringList">
                    <items>
                      <item
                        translatable="yes"
                        comments="We support versions 3 and 5 of Cangjie. Most people will think version 3 is the 'standard' one as it is the one used on Windows"
                      >Standard (Version 3)</item>
                      <item
                        translatable="yes"
                        comments="We support versions 3 and 5 of Cangjie. Version 5 is the newer, supposedly better version, but most people use version 3."
                      >Updated (Version 5)</item>
                    </items>
                  </object>
                </property>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="GtkSeparator">
          </object>
        </child>
        <child>
          <object class="GtkText" id="title-user-interface">
            <property name="text" translatable="yes">Controls and Interface</property>
            <property name="xalign">0</property>
            <style>
                <class name="subtitle"/>
            </style>
          </object>
        </child>
        <child>
          <object class="GtkCheckButton" id="input-mode-hotkey-shift">
            <property name="valign">start</property>
            <property
              name="tooltip-text"
              translatable="yes"
            >When enabled, you may quickly tab on the SHIFT key to toggle between Chinese input mode and the Direct (English / Number) input mode.</property>
            <child>
              <object class="GtkBox">
                <property name="orientation">vertical</property>
                <property name="spacing">4</property>
                <property name="margin-start">10</property>
                <child>
                  <object class="GtkLabel">
                    <property name="halign">start</property>
                    <property
                      name="label"
                      translatable="yes"
                    >Input Mode Hotkey</property>
                  </object>
                </child>
                <child>
                  <object class="GtkLabel">
                    <property name="halign">start</property>
                    <property
                      name="label"
                      translatable="yes"
                      comments="Describe the SHIFT hotkey feature"
                    >Enable using SHIFT key to toggle the input mode (Chinese mode / Direct mode)</property>
                    <attributes>
                      <attribute name="foreground" value="#88888a8a8484"/>
                    </attributes>
                  </object>
                </child>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="GtkCheckButton" id="star-helps-learners">
            <property name="valign">start</property>
            <property
              name="tooltip-text"
              translatable="yes"
            >When typing with wildcard, show complete radicals of the candidate characters when typing.</property>
            <child>
              <object class="GtkBox">
                <property name="orientation">vertical</property>
                <property name="spacing">4</property>
                <property name="margin-start">10</property>
                <child>
                  <object class="GtkLabel">
                    <property name="halign">start</property>
                    <property
                      name="label"
                      translatable="yes"
                    >Learner assists for wildcard candidates</property>
                  </object>
                </child>
                <child>
                  <object class="GtkLabel">
                    <property name="halign">start</property>
                    <property
                      name="label"
                      translatable="yes"
                      comments="Describe the SHIFT hotkey feature"
                    >When enabled and when typing with wildcard (*), you will see the radicals of candidates after the actual character.</property>
                    <attributes>
                      <attribute name="foreground" value="#88888a8a8484"/>
                    </attributes>
                  </object>
                </child>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="GtkSeparator">
          </object>
        </child>
        <child>
          <object class="GtkText" id="title-character-support">
            <property name="text" translatable="yes">Extended character support</property>
            <property name="xalign">0</property>
            <style>
                <class name="subtitle"/>
            </style>
        </object>
        </child>
        <child>
          <object class="GtkLabel" id="label2">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="xalign">0</property>
            <property name="margin-top">10</property>
            <property name="margin-bottom">5</property>
            <property
              name="label"
              translatable="yes"
              comments="This is just some text to explain what the other options do."
            >Only characters common in Hong Kong can be inputted by default.
The following additional characters can be enabled:</property>
          </object>
        </child>
        <child>
          <object class="GtkCheckButton" id="include-zhuyin">
            <property
              name="tooltip-text"
              translatable="yes"
            >Taiwan phonetic symbols of Mandarin Chinese</property>
            <child>
              <object class="GtkBox">
                <property name="orientation">vertical</property>
                <property name="spacing">4</property>
                <property name="margin-start">10</property>
                <child>
                  <object class="GtkLabel">
                    <property name="halign">start</property>
                    <property
                      name="label"
                      translatable="yes"
                      comments="The Taiwanese Zhuyin alphabet is known under two names in English. Obviously, if it only has one name in your language, don't try to keep two alternative names here."
                    >Zhuyin / Bopomofo</property>
                  </object>
                </child>
                <child>
                  <object class="GtkLabel">
                    <property name="halign">start</property>
                    <property
                      name="label"
                      translatable="yes"
                      comments="This string includes a list of examples. Do not translate the characters themselves, only translate it so it looks like a list of examples in your own language, but keep these characters as examples."
                    >Eg. ㄅ, ㄆ, ㄇ, ㄈ</property>
                    <attributes>
                      <attribute name="foreground" value="#88888a8a8484"/>
                    </attributes>
                  </object>
                </child>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="GtkCheckButton" id="include-jp">
            <property
              name="tooltip-text"
              translatable="yes"
            >Hiragana, Katakana, and characters that only exist in Kanji</property>
            <child>
              <object class="GtkBox">
                <property name="orientation">vertical</property>
                <property name="spacing">4</property>
                <property name="margin-start">10</property>
                <child>
                  <object class="GtkLabel">
                    <property name="halign">start</property>
                    <property
                      name="label"
                      translatable="yes"
                    >Japanese</property>
                  </object>
                </child>
                <child>
                  <object class="GtkLabel">
                    <property name="halign">start</property>
                    <property
                      name="label"
                      translatable="yes"
                      comments="This string includes a list of examples. Do not translate the characters themselves, only translate it so it looks like a list of examples in your own language, but keep these characters as examples."
                    >Eg. あ, ア, 鉄</property>
                    <attributes>
                      <attribute name="foreground" value="#88888a8a8484"/>
                    </attributes>
                  </object>
                </child>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="GtkCheckButton" id="include-allzh">
            <property name="valign">start</property>
            <property
              name="tooltip-text"
              translatable="yes"
            >Chinese characters that are rarely used or only exists in Simplifed Chinese</property>
            <child>
              <object class="GtkBox">
                <property name="orientation">vertical</property>
                <property name="spacing">4</property>
                <property name="margin-start">10</property>
                <child>
                  <object class="GtkLabel">
                    <property name="halign">start</property>
                    <property
                      name="label"
                      translatable="yes"
                      comments="If they enable this option, we will return to the users some rarer Chinese characters which they most likely don't need, in addition to the ones we return by default. It should be clear that most users don't need to enable this option."
                    >Rare Chinese characters</property>
                  </object>
                </child>
                <child>
                  <object class="GtkLabel">
                    <property name="halign">start</property>
                    <property
                      name="label"
                      translatable="yes"
                      comments="This string includes a list of examples. Do not translate the characters themselves, only translate it so it looks like a list of examples in your own language, but keep these characters as examples."
                    >Eg. 诉, 䜣, 㻹</property>
                    <attributes>
                      <attribute name="foreground" value="#88888a8a8484"/>
                    </attributes>
                  </object>
                </child>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="GtkCheckButton" id="include-symbols">
            <property name="valign">start</property>
            <property
              name="tooltip-text"
              translatable="yes"
            >Various obscured symbols like arrows, numbers with bracket and etc.</property>
            <child>
              <object class="GtkBox">
                <property name="orientation">vertical</property>
                <property name="spacing">4</property>
                <property name="margin-start">10</property>
                <child>
                  <object class="GtkLabel">
                    <property name="halign">start</property>
                    <property
                      name="label"
                      translatable="yes"
                    >Symbols</property>
                  </object>
                </child>
                <child>
                  <object class="GtkLabel">
                    <property name="halign">start</property>
                    <property
                      name="label"
                      translatable="yes"
                      comments="This is a list of examples. Do not translate the characters themselves, only translate it so it looks like a list of examples in your own language, but keep these characters as examples."
                    >Eg. ○, ㈤, →</property>
                    <attributes>
                      <attribute name="foreground" value="#88888a8a8484"/>
                    </attributes>
                  </object>
                </child>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="GtkBox">
            <property name="halign">end</property>
            <property name="margin-top">8</property>
            <child>
              <object class="GtkButton" id="close-button">
                <property
                  name="label"
                  translatable="yes"
                >Close</property>
              </object>
            </child>
          </object>
        </child>
      </object>
    </child>
  </object>
  <object class="AdwAboutDialog" id="about_dialog">
    <property name="application_name" translatable="yes">IBus Cangjie</property>
    <property name="developer_name" translatable="yes">IBus Cangjie developers</property>
    <property name="website">https://cangjie.pages.freedesktop.org/</property>
    <property name="issue_url">https://gitlab.freedesktop.org/cangjie/ibus-cangjie/-/issues</property>
    <property name="copyright" translatable="yes">© 2012 IBus Cangjie Developers</property>
  </object>
</interface>
