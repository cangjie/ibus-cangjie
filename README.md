# ibus-cangjie

[![license-shield]][license-url]
[![Code style: black][codestyle-shield]][codestyle-url]

[CI]: https://gitlab.freedesktop.org/Cangjians/ibus-cangjie/badges/master/pipeline.svg
[license-url]: http://www.gnu.org/licenses/gpl.html
[license-shield]: https://img.shields.io/gitlab/license/cangjie%2Fibus-cangjie?gitlab_url=https%3A%2F%2Fgitlab.freedesktop.org&color=blue
[codestyle-url]: https://github.com/psf/black
[codestyle-shield]: https://img.shields.io/badge/code%20style-black-000000.svg

This is an IBus engine for users of the Cangjie and Quick input methods.

It is primarily intended to Hong Kong people who want to input Traditional
Chinese, as they are (by far) the majority of Cangjie and Quick users.

However, it should work for others as well (e.g to input Simplified Chinese).

Note that the actual input logic is handled by
[libcangjie](https:////gitlab.freedesktop.org/Cangjie/ibus-cangjie).

Development happens [on Gitlab](https://gitlab.freedesktop.org/Cangjie/ibus-cangjie),
table release tarballs can be found in
[the download section](https://cangjie.pages.freedesktop.org/downloads/ibus-cangjie/).


## Installation

Please reference [Installing IBus Cangjie][website-install-page] on our website.

[website-install-page]: https://cangjie.pages.freedesktop.org/projects/ibus-cangjie/install.html

## Contributing

Free software can only thrive thanks to people like you who take the
time to help. We appreciate all sorts of help, including but not limited to:

* Coding New Feature or Bug Fixes
* Issue Report
* Testing
* Translation
* Distribution

Please read our [Contributing docs](CONTRIBUTING.md) for details.

## Tweak it Your Own

You are free to modify IBus Cangjie to your liking. You may find instructions
of how to work with our source code in the [Contributing docs](CONTRIBUTING.md).

## Legalities

IBus Cangjie is offered under the terms of the
[GNU General Public License, either version 3 or any later version](http://www.gnu.org/licenses/gpl.html).

We won't ask you to sign a copyright assignment or any other kind of silly and
tedious legal document, so just send us patches and/or pull requests!
