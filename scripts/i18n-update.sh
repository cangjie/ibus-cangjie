#!/bin/bash

set -x

requires_command() {
    if
        ! which "$1" >/dev/null 2>&1
    then
        # Return error message.
        >&2 echo "This script requires the \"$1\" command tool to run. Please install it first."
        exit 1
    fi
}

# Relative path to this folder
CURRENT_PWD=$(pwd)

# Detect source root from meson's variable or by
# the command $0 variable.
SOURCE_ROOT=${MESON_SOURCE_ROOT:-"$(dirname $0)/abc"}
DESCRIPTIVE_TITLE=${PROJECT_DESCRIPTIVE_TITLE:-"SOME DESCRIPTIVE TITLE."}
NAME=${PROJECT_NAME:-"PROJECT_NAME"}
VERSION=v${PROJECT_VERSION:-"1.0"}
COPYRIGHT=${PROJECT_COPYRIGHT:-"COPYRIGHT_HOLDERS"}
START_YEAR=${PROJECT_START_YEAR:-"1970"}
YEAR="$START_YEAR-$(date +%Y) -"

# Check dependencies
requires_command xgettext
requires_command msgmerge
requires_command sed

# Go to the correct folder
cd "$SOURCE_ROOT"

# Update .pot file with the listed files in POTFILES.in
xgettext \
    --join-existing \
    --files-from=./po/POTFILES.in \
    --copyright-holder="$COPYRIGHT" \
    --package-name="$NAME" \
    --package-version="$VERSION" \
    --sort-by-file \
    --keyword=_ \
    --keyword=N_ \
    --output=./po/ibus-cangjie.pot

# Extract the first line (descriptive title) from backup
# .pot file and use it for the new .pot file.
sed -i "0,/YEAR/{s/YEAR/$YEAR/}" ./po/ibus-cangjie.pot
sed -i "0,/YEAR/{s/SOME DESCRIPTIVE TITLE./$DESCRIPTIVE_TITLE/}" ./po/ibus-cangjie.pot

# Remove the work file and bak file
#rm ./po/ibus-cangjie.pot.work ./po/ibus-cangjie.pot.bak

# Show diff
git diff ./po/ibus-cangjie.pot
