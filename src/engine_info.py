# Copyright (c) 2024 - The IBus Cangjie authors
#
# This file is part of ibus-cangjie, the IBus Cangjie input method engine.
#
# ibus-cangjie is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ibus-cangjie is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ibus-cangjie.  If not, see <http://www.gnu.org/licenses/>.

__all__ = ["EngineInfo", "EngineInfoManager"]

from typing import List
import xml.dom.minidom as md
import gettext


# FIXME: Find a way to de-hardcode the gettext package
def _(x):
    return gettext.dgettext("ibus-cangjie", x)


class EngineInfo(object):
    """Engine information class.

    This class contains the information of an engine, including its name,
    longname, description, language, layout, symbol, icon, license, author,
    setup command, and rank.
    """

    name: str = ""
    longname: str = ""
    description: str = ""
    language: str = ""
    layout: str = ""
    symbol: str = ""
    icon: str = ""
    license: str = ""
    author: str = ""
    setup: str = ""
    rank: int = 0

    def __init__(
        self,
        name: str,
        longname: str,
        description: str,
        language: str,
        layout: str,
        symbol: str,
        icon: str,
        license: str,
        author: str,
        setup: str,
        rank: int,
        icon_prop_key: str,
    ) -> None:
        self.name = name
        self.longname = longname
        self.description = description
        self.language = language
        self.layout = layout
        self.symbol = symbol
        self.license = license
        self.author = author
        self.icon = icon
        self.setup = setup
        self.rank = rank
        self.icon_prop_key = icon_prop_key

    def xml(self) -> str:
        # output XML of the engine information
        return f"""
        <engine>
            <name>{self.name}</name>
            <longname>{self.longname}</longname>
            <description>{self.description}</description>
            <language>{self.language}</language>
            <layout>{self.layout}</layout>
            <symbol>{self.symbol}</symbol>
            <icon>{self.icon}</icon>
            <license>{self.license}</license>
            <author>{self.author}</author>
            <setup>{self.setup}</setup>
            <rank>{self.rank}</rank>
            <icon_prop_key>{self.icon_prop_key}</icon_prop_key>
        </engine>
        """


class EngineInfoManager(object):
    """Engine information manager class.

    This class manages the engine information of all engines and
    provides way to select and display the information of engines
    in iBus component <engines> XML format.

    This is for supporting dynamic engine information in iBus so
    the engine information (most notably the longname and symbol
    that shows in input method selection menu) can be localized.
    """

    engine_info_list: List[EngineInfo] = []

    def __init__(self, icondir: str, bindir: str) -> None:
        """Initialize the engine information manager with the engine
        information.

        The engine information is hardcoded in this method. The icondir and
        bindir are the directories where the icons and setup commands are
        located. These parameters would be generated into the entry script
        by the build process so they matches the installation setup.

        (See "scripts/ibus-engine-cangjie.in)

        Args:
            icondir: The directory where the icons are located.
            bindir: The directory where the setup commands are located.
        """
        self.engine_info_list = [
            EngineInfo(
                name="cangjie",
                longname=_("Cangjie"),
                description=_("Cangjie Input Method"),
                language="zh_HK",
                layout="us",
                symbol="倉頡",
                icon=f"{icondir}/cangjie.png",
                license="GPLv3+",
                author="The IBus Cangjie authors",
                setup=f"{bindir}/ibus-setup-cangjie cangjie",
                rank=0,
                icon_prop_key="InputMode",
            ),
            EngineInfo(
                name="quick",
                longname=_("Quick"),
                description=_("Quick Input Method"),
                language="zh_HK",
                layout="us",
                symbol="速成",
                icon=f"{icondir}/quick.png",
                license="GPLv3+",
                author="The IBus Cangjie authors",
                setup=f"{bindir}/ibus-setup-cangjie quick",
                rank=0,
                icon_prop_key="InputMode",
            ),
        ]

    def select_engine_by_name(self, name: str) -> List[EngineInfo]:
        """Return the information of the engine with the given name.

        Args:
            name: The name of the engine to select.

        Returns:
            The information of the engine with the given name.
        """
        for engine_info in self.engine_info_list:
            if engine_info.name == name:
                return [engine_info]
        return []

    def all_engines(self) -> List[EngineInfo]:
        """Return the information of all engines.

        Returns:
            The information of all engines.
        """
        return self.engine_info_list

    def format_engines_xml(self, engines: List[EngineInfo]) -> str:
        """Return the engine information in iBus component <engines> XML format.

        See:
            - "data/org.freedesktop.cangjie.ibus.Cangjie.xml.in"
            - "data/org.freedesktop.cangjie.ibus.Quick.xml.in"

        Args:
            engines: The list of engines to format into XML.

        Returns:
            The iBus component <engines> XML format.
        """
        xml_string = "<engines>"
        for engine in engines:
            xml_string += engine.xml()
        xml_string += "</engines>"

        def pretty_print(f):
            return "\n".join(
                [
                    line
                    for line in md.parseString(xml_string)
                    .childNodes[0]
                    .toprettyxml(indent=" " * 2, standalone=False)
                    .split("\n")
                    if line.strip()
                ]
            )

        return pretty_print(xml_string)
