# Copyright (c) 2012-2013 - The IBus Cangjie authors
#
# This file is part of ibus-cangjie, the IBus Cangjie input method engine.
#
# ibus-cangjie is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ibus-cangjie is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ibus-cangjie.  If not, see <http://www.gnu.org/licenses/>.

import gi  # type: ignore

gi.require_version("GSound", "1.0")

from gi.repository import GSound  # type: ignore


class ErrorSound:
    def __init__(self):
        self.ctx = GSound.Context()
        self.ctx.init()

    def play_error(self):
        self.ctx.play_simple({GSound.ATTR_EVENT_ID: "dialog-error"})
