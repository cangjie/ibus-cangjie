# Contributing

Thanks for your interest in contributing to ibus-cangjie. :grin:
Free software can only thrive thanks to people like you who take the
time to help.

The very first thing you need to do is accept our [code of conduct][coc]
(we took it from [Freedesktop.org][freedesktop-coc]).

[freedesktop-coc]: https://www.freedesktop.org/wiki/CodeOfConduct/
[coc]: code-of-conduct.md

<details>
<summary>Table of Contents</summary>

- [Contribution Guidelines](#contribution-guidelines)
  - [Filing issues](#filing-issues)
  - [Submitting changes](#submitting-changes)
    - [Commit history](#commit-history)
    - [Commit messages](#commit-messages)
- [Working with Source Code](#working-with-source-code)
  - [Build and Install](#build-and-install)
  - [After Installation](#after-installation)
  - [Debug](#debug)
  - [Unit Test](#unit-test)
  - [Translation Updates](#translation-updates)
- [Distribution](#distribution)

</details>

## Contribution Guidelines

Following these guidelines helps to communicate that you respect the time of
the developers managing and developing this project. In return, they should
reciprocate that respect in addressing your issue, assessing changes, and
helping you finalize your contributions.

### Filing issues

Whenever you experience a problem with this software, please let us know so we
can make it better.

But first, take the time to search through the list of [existing issues] to see
whether it is already known.

Be sure to provide all the information you can, as that will really help us
fixing your issue as quickly as possible.

[existing issues]: https://gitlab.freedesktop.org/cangjie/ibus-cangjie/-/issues

### Submitting changes

If you want to implement a new feature, make sure you
[first open an issue][creating issue] to discuss it and ensure the feature
is desired.

We accept changes through the usual merge request process.

[creating issue]: https://gitlab.freedesktop.org/cangjie/ibus-cangjie/-/issues/new

#### Commit history

Try to keep your branch split into atomic commits.

For example, if you made a first commit to implement a change, then a second
one to fix a problem in the first commit, squash them together.

If you need help doing that, please mention it in your merge request and we
will guide you towards using Git to achieve a nice and clean branch history. :smile:

#### Commit messages

Commit messages are extremely important. They tell the story of the project,
how it arrived to where it is now, why we took the decisions that made it the
way it is.

Chris Beams wrote [a wonderful post](https://chris.beams.io/posts/git-commit/)
that we highly recommend you read. The highlight of the post are 7 rules we ask
you to try and follow:

1.  Separate subject from body with a blank line
2.  Limit the subject line to 50 characters
3.  Capitalize the subject line
4.  Do not end the subject line with a period
5.  Use the imperative mood in the subject line
6.  Wrap the body at 72 characters
7.  Use the body to explain what and why vs. how

We further want to add our own rules:

*   if the commit is related to an issue, please mention the id of that issue
    in the commit message; for example, use something like `Fixes #123` or
    `Implements #123`.


## Working with Source Code

If you want to work on the source code, this is a short guideline for it:

Before working on the source code, please make sure the following
dependencies are installed:

* [libcangjie][libcangjie] (see: [Installing libcangjie](https://cangjie.pages.freedesktop.org/projects/libcangjie/install.html))
* [pycangjie][pycangjie] (see: [Installing pycangjie](https://cangjie.pages.freedesktop.org/projects/pycangjie/install.html))
* [meson][meson]
* [ibus][ibus]
* [python3][python]

[libcangjie]: https://gitlab.freedesktop.org/cangjie/libcangjie/
[pycangjie]: https://gitlab.freedesktop.org/cangjie/pycangjie/
[meson]: https://mesonbuild.com/
[ibus]: https://github.com/ibus/ibus
[python]: https://www.python.org/

You can get the latest sources from our Git repository:

```bash
git clone https://gitlab.freedesktop.org/cangjie/ibus-cangjie.git
```

### Build and Install

To cleanly setup the project for build with meson:

```bash
meson setup --prefix=/usr --wipe ./builddir
```

To generate the resources out of the sources for this setup:

```bash
meson compile -C ./builddir
```

To install _(**Caution: This will overwrite all existing package-installed files**)_:

```bash
meson install -C ./builddir
```

Supported platforms are Fedora and Ubuntu, that's what we test in our CI. If
the tests fail on another platform then do let us know and we'll see whether we
can add it to our CI so it gets tested as well. :smile:

You can also generate your own tarball:

```bash
meson dist -C builddir
```

### After Installation

Everytime after installation, you must kill all instance of running ibus-engine-cangjie:
```bash
ibus restart
```

Then you may simply switch your input method to re-activate ibus-cangjie.


### Debug

With the above installation setup, our input method starts with a script
`/usr/libexec/ibus-engine-cangjie`, which will starts a loop with our
`Engine` instance, connect it to IBus for further key events or other
signals.

To see messages, STDERR or other potential errors from the
`ibus-engine-cangjie` instance:

1. First, __make sure your are NOT using__ either Cangjie or Quick
   implementations of IBus Cangjie.

   This is important because if not, ibus daemon will automatically restore
   previous input method (IBus Cangjie), which will immediately start a new
   background instance of `ibus-engine-cangjie` that you cannot monitor.

2. Restart ibus, which will stop all IBus input engine (includes IBus
   Cangjie) instances:

   ```bash
   ibus restart
   ```

3. Start `ibus-engine-cangjie` in foreground.

   For Cangjie:
   ```bash
   /usr/libexec/ibus-engine-cangjie --ibus cangjie
   ```

   For Quick:
   ```bash
   /usr/libexec/ibus-engine-cangjie --ibus quick
   ```


### Unit Test

Our unit tests are based on Python's own [unittest][python-unittest]
module. It is runnable directly from source.

For instructions, please read [README.md](tests/README.md) in the tests
folder.

[python-unittest]: https://docs.python.org/3/library/unittest.html


### Translation Updates

If your work involves adding new translation strings, you need to update
[translation template file](po/ibus-cangjie.pot) for the change. Run this:

```bash
meson setup ./builddir && ninja -C ./builddir i18n-update
```

## Distribution

We are eager to expand our support to different distributions (see
[this page][install-ibus-cangjie] for currently supported distros). But
we are a very small team. Please understand that we do not use all distros
day-to-day.

If you have experience packaging or even distributing to new Linux distros,
please [create a new issue][creating issue]. We can provide any help that
you need. And we would love to update [our website][website] for a new
installation instruction.

[install-ibus-cangjie]: https://cangjie.pages.freedesktop.org/projects/ibus-cangjie/install.html
[website]: https://cangjie.pages.freedesktop.org/