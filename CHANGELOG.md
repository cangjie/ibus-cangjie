# Changelog

All notable changes to this project will be documented in this file.


## [Unreleased]

### Fixed

- Fixed setting change caused issue happened during the setting values migration.
  ([Koala Yeung](@yookoala))

### Changed

- Refactor Engine methods to only reference the lookup table UI through the
  lookuptable attribute. ([Koala Yeung](@yookoala))


## [2.5] - 2024-11-20

### Added

- ibus-cangjie will now display the code to type when entering a wildcard in
  the code. [Contribution from 2017 by NCHL on github](https://github.com/Cangjians/ibus-cangjie/pull/84#issue-213946874). ♥️
- The code is now type checked, to avoid future mistakes. ([Koala Yeung](@yookoala))
- Wrote some contribution guidelines. ([Koala Yeung](@yookoala))
- Added/improved the zh_HK translations. ([Koala Yeung](@yookoala))
- Added a new zh_CN translation. ([Koala Yeung](@yookoala))
- Added an about dialog to show support information.
  (*Note: now ibus-cangjie depends on libadwaita 1.5+*)
  ([Koala Yeung](@yookoala))

### Changed

- The minimum meson version is now 1.3.2, which is in Ubuntu LTS 24.04.
  ([Mathieu Bridon][bochecha])
- Improved the restart procedure. ([Koala Yeung](@yookoala))
- Used the prefered / operator instead of the deprecated join_paths function in
  the meson buildsystem files. ([Mathieu Bridon](@bochecha))
- Improved the unit testing. ([Koala Yeung](@yookoala))
- ibus-cangjie is now properly installed into libexec dir, as requested by our
  Debian friends. ([Koala Yeung](@yookoala))
- Code is now passed through a linter, eiter manually or in the CI.
  ([Koala Yeung](@yookoala))
- Added a passthrough mode (英數模式) with a simple shortcut to access.
  ([Koala Yeung](@yookoala))
- Properly display the translated name of the input method in input method
  selection dialog. ([Koala Yeung](@yookoala))


## [2.4] - 2014-03-30

### Fixed

- Fixes a major annoyance with incorrect candidate popup positioning.
  ([Mathieu Bridon](@bochecha))


## [2.3] - 2015-03-04

This is a bug fix release.

### Fixed

- Deduplicate characters ([Mathieu Bridon](@bochecha))
- Fix some issues triggering crash catchers like Fedora's ABRT or Ubuntu's
  Apport ([Mathieu Bridon](@bochecha))
- Fix a mistake in the Taiwan translation ([Koala Yeung](@yookoala))
- Fix a deprecation warning ([Mathieu Bridon](@bochecha))
- Properly detect graphical capabilities, to skip the appropriate unit tests
  ([Mathieu Bridon](@bochecha))

### Changed

- Added Gentoo installation instructions ([Brendan Horan][brendanhoran])


## [2.2] - 2014-04-25

### Fixed

- Fixed using the numpad to commit candidates. ([Mathieu Bridon](@bochecha))

### Changed

- Docs Improvements
  - Added installation instructions for Arch Linux. ([Antony Ho][anthonyho])
  - Removed an obsolete (and confusing) notice about release tarballs. ([Mathieu Bridon](@bochecha))
- Testing improvements
  - Get Travis CI to build against the 1.x branch of libcangjie and pycangjie,
    as their master branch has now broken the API in a way that ibus-cangjie
    1.x doesn't build against it any more. ([Mathieu Bridon](@bochecha))
  - Use the in-memory GSettings backend for the tests, so avoid any undesired
    side-effects from the running environment. ([Mathieu Bridon](@bochecha))


## [2.1] - 2014-02-02

- Add a link to the release tarballs in the README ([Mathieu Bridon](@bochecha))
- Actually run the unit tests on « make check » ([Anthony Wong][anthonywong])
- Improve « make clean » ([Anthony Wong][anthonywong])
- Add install instructions for a few distributions ([Mathieu Bridon](@bochecha))
- Improve the optionality of pycanberra ([Mathieu Bridon](@bochecha))
- Fix the pt_BR translation ([Mathieu Bridon](@bochecha))
- Reset sys.path to limit side effects on the unit tests ([Mathieu Bridon](@bochecha))
- Ensure validity of the preferences UI file ([Mathieu Bridon](@bochecha))
- Fix the UI file ([Mathieu Bridon](@bochecha))


## [2.0] - 2013-12-22

The first stable release of ibus-cangjie from the Cangjians.


## [1.0] - 2013-05-02

The very first release of ibus-cangjie.

Goes hand in hand with Wan Leung Wong's libcangjie, not the Cangjians' one.

This shouldn't be used any more.

[anthonyho]: https://github.com/antonyho
[anthonywong]: https://github.com/anthonywong
[bochecha]: https://gitlab.freedesktop.org/bochecha
[brendanhoran]: https://github.com/brendanhoran
[dridi]: https://github.com/dridi
[linquize]: https://github.com/linquize
[wanleung]: https://github.com/wanleung
[yookoala]: https://gitlab.freedesktop.org/yookoala

[Unreleased]: https://gitlab.freedesktop.org/cangjie/ibus-cangjie/-/compare/v2.5...HEAD
[2.5]: https://gitlab.freedesktop.org/cangjie/ibus-cangjie/-/compare/v2.4...v2.5
[2.4]: https://gitlab.freedesktop.org/cangjie/ibus-cangjie/-/compare/v2.3...v2.4
[2.3]: https://gitlab.freedesktop.org/cangjie/ibus-cangjie/-/compare/v2.2...v2.3
[2.2]: https://gitlab.freedesktop.org/cangjie/ibus-cangjie/-/compare/v2.1...v2.2
[2.1]: https://gitlab.freedesktop.org/cangjie/ibus-cangjie/-/compare/v2.0...v2.1
[2.0]: https://gitlab.freedesktop.org/cangjie/ibus-cangjie/-/compare/v1.0...v2.0
[1.0]: https://gitlab.freedesktop.org/cangjie/ibus-cangjie/-/tags/v1.0

---

Note: This document trys to follow the [keepachangelog.com][keepachangelog]
suggested format.

[keepachangelog]: https://keepachangelog.com/en/1.0.0/


