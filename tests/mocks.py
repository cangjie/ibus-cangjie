# Copyright (c) 2013 - The IBus Cangjie authors
#
# This file is part of ibus-cangjie, the IBus Cangjie input method engine.
#
# ibus-cangjie is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ibus-cangjie is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ibus-cangjie.  If not, see <http://www.gnu.org/licenses/>.


from __future__ import annotations
from typing import List
from gi import require_version  # type: ignore

require_version("IBus", "1.0")
from gi.repository import IBus  # type: ignore

__all__ = [
    "MockEngine",
    "MockLookupTable",
    "MockPropList",
    "MockProperty",
    "MockErrorSound",
]


class MockEngine:
    def __init__(self):
        self._mock_auxiliary_text = ""
        self._mock_committed_text = ""

    def update_auxiliary_text(self, text, visible):
        self._mock_auxiliary_text = text.text

    def commit_text(self, text):
        self._mock_committed_text += text.text

    def update_lookup_table(self, table, visible):
        # We don't need anything here for the unit tests
        pass

    def update_preedit_text(self, text, cursor_pos, visible):
        # We don't need anything here for the unit tests
        #
        # We only have this for an ugly workaround.
        pass


class MockLookupTable:

    _mock_candidates: List[str] = []
    _mock_cursor_pos: int = 0
    _mock_page_size: int = 9
    _mock_round: int
    _mock_orientation: int

    def __init__(self):
        self.clear()

    def clear(self):
        self._mock_candidates = []
        self._mock_cursor_pos = 0

    def set_page_size(self, size):
        self._mock_page_size = size

    def get_page_size(self) -> int:
        return self._mock_page_size

    def set_round(self, round):
        self._mock_round = round

    def set_orientation(self, orientation):
        self._mock_orientation = orientation

    def append_candidate(self, candidate):
        self._mock_candidates.append(candidate)

    def get_cursor_pos(self):
        return self._mock_cursor_pos

    def get_candidate(self, index):
        return self._mock_candidates[index]

    def get_number_of_candidates(self):
        return len(self._mock_candidates)

    def page_down(self):
        self._mock_cursor_pos += self._mock_page_size


class MockPropList:
    def append(self, property):
        # We don't need anything here for the unit tests
        pass


class MockProperty:
    """Mock IBus.Property class for unit tests.

    Slots and methods are based on the IBus.Property class from IBus

    see: https://ibus.github.io/docs/ibus-1.5/IBusProperty.html
    """

    __slots__ = (
        "key",
        "prop_type",
        "label",
        "symbol",
        "icon",
        "tooltip",
        "state",
        "sensitive",
        "visible",
        "sub_props",
    )

    def __init__(self, *args, **kwargs):
        # We don't need anything here for the unit tests
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)
            else:
                raise AttributeError(f"Unknown attribute {key}")
        pass

    def get_key(self) -> str:
        return self.key  # type: ignore

    def get_prop_type(self) -> IBus.PropType:
        return self.prop_type  # type: ignore

    def set_label(self, label: IBus.Text):
        self.label = label

    def get_label(self) -> IBus.Text:
        return self.label

    def set_symbol(self, symbol: IBus.Text):
        self.symbol = symbol

    def get_symbol(self) -> IBus.Text:
        return self.symbol

    def set_icon(self, icon: str):
        self.icon = icon

    def get_icon(self) -> str:
        return self.icon

    def set_tooltip(self, tooltip: IBus.Text):
        self.tooltip = tooltip

    def get_tooltip(self) -> IBus.Text:
        return self.tooltip

    def set_sensitive(self, sensitive: bool):
        self.sensitive = sensitive

    def get_sensitive(self) -> bool:
        return self.sensitive

    def set_visible(self, visible: bool):
        self.visible = visible

    def get_visible(self) -> bool:
        return self.visible

    def set_state(self, state: IBus.PropState):
        self.state = state

    def get_state(self) -> IBus.PropState:
        return self.state


class MockErrorSound:
    def __init__(self):
        self._mock_played_events = []

    def play_error(self):
        self._mock_played_events.append("error")
