import unittest
from gi.repository import Gio, GLib  # type: ignore

from ibus_cangjie.engine import EngineCangjie, EngineQuick, legacy_schema_migration  # type: ignore


class UnsupportedType(Exception):
    pass


class MigrationTestCase(unittest.TestCase):
    def setUp(self):
        self.engine = EngineCangjie()

    def tearDown(self):
        del self.engine

    def test_legacy_schema_migration_absent_schema(self):
        schemas = Gio.SettingsSchemaSource.get_default()
        absent_schema = schemas.lookup("schema-does-not-exist", True)

        self.assertIsNone(absent_schema, "there should be no schema by this name")

    def test_legacy_schema_migration_existing_schema_absent_key(self):
        schemas = Gio.SettingsSchemaSource.get_default()
        legacy_schema = schemas.lookup("org.cangjians.ibus.cangjie", True)
        self.assertIsNotNone(legacy_schema, "found legacy schema to migrate")

        self.assertFalse(legacy_schema.has_key("key-does-not-exist"))

    def test_legacy_schema_migration_existing_schema_key_types(self):
        schemas = Gio.SettingsSchemaSource.get_default()
        legacy_schema_id = "org.cangjians.ibus.cangjie"
        legacy_schema = schemas.lookup(legacy_schema_id, True)
        self.assertIsNotNone(legacy_schema, "found legacy schema to migrate")

        legacy_settings = Gio.Settings(schema_id=legacy_schema_id)
        for key in legacy_schema.list_keys():
            self.assertTrue(legacy_schema.has_key(key))
            value_type = legacy_schema.get_key(key).get_value_type()

            if value_type.equal(GLib.VariantType.new("s")):
                self.assertIsInstance(legacy_settings.get_string(key), str)

            elif value_type.equal(GLib.VariantType.new("i")):
                self.assertIsInstance(legacy_settings.get_int(key), int)

            elif value_type.equal(GLib.VariantType.new("b")):
                self.assertIsInstance(legacy_settings.get_boolean(key), bool)

            else:
                with self.assertRaises(UnsupportedType) as exc:
                    raise UnsupportedType(key)
