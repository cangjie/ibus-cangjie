# Copyright (c) 2013 - The IBus Cangjie authors
#
# This file is part of ibus-cangjie, the IBus Cangjie input method engine.
#
# ibus-cangjie is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ibus-cangjie is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ibus-cangjie.  If not, see <http://www.gnu.org/licenses/>.


from __future__ import annotations
import os
import subprocess
import sys
import unittest

from .mocks import (
    MockEngine,
    MockLookupTable,
    MockProperty,
    MockPropList,
    MockErrorSound,
)


# -- Make sure we test the development tree ----------------------------------
script_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, script_path)
import src

sys.modules["ibus_cangjie"] = src
sys.path.pop(0)

# This is required for the next mock to work
import ibus_cangjie.sounds  # type: ignore # noqa: F401

# -- Monkey patch the environment with the mock classes ----------------------
sys.modules["gi.repository.IBus"].Engine = MockEngine  # type: ignore
sys.modules["gi.repository.IBus"].LookupTable = MockLookupTable  # type: ignore
sys.modules["gi.repository.IBus"].Property = MockProperty  # type: ignore
sys.modules["gi.repository.IBus"].PropList = MockPropList  # type: ignore

sys.modules["ibus_cangjie.sounds"].ErrorSound = MockErrorSound  # type: ignore


# -- Set some environment variables for the tests ----------------------------
# The unit tests will need this to find the settings schemas
os.environ["GSETTINGS_BACKEND"] = "memory"
os.environ["GSETTINGS_SCHEMA_DIR"] = f"{script_path}/data"
subprocess.check_call(["glib-compile-schemas", os.environ["GSETTINGS_SCHEMA_DIR"]])

# The unit tests will need this to find the preferences UI
os.environ["PREFERENCES_UI_DIR"] = f"{script_path}/data"


# -- Load and run our unit tests ---------------------------------------------
if __name__ == "__main__":
    verbosity = 2
    # Use color_runner if available. Otherwise, fall back to the default runner.
    try:
        import colour_runner.result  # type: ignore
        import colour_runner.runner  # type: ignore
        from blessings import Terminal  # type: ignore

        class CustomTextResult(colour_runner.result.ColourTextTestResult):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)

                try:
                    # Try force styling to work in CI
                    self._terminal = Terminal(force_styling=True)
                except Exception:
                    self._terminal = Terminal()
                self.colours = {
                    None: str,
                    "error": self._terminal.bold_red,
                    "expected": self._terminal.blue,
                    "fail": self._terminal.bold_yellow,
                    "skip": str,
                    "success": self._terminal.green,
                    "title": self._terminal.blue,
                    "unexpected": self._terminal.bold_red,
                }

        runner = colour_runner.runner.ColourTextTestRunner(verbosity=verbosity)
        runner.resultclass = CustomTextResult
    except ModuleNotFoundError as e:
        print(
            f"Unable to import colour_runner ({e}).\nFalling back to default runner.\n",
            file=sys.stderr,
        )
        runner = unittest.TextTestRunner(verbosity=verbosity)

    loader = unittest.TestLoader()
    suite = loader.discover(f"{script_path}/tests")
    result = runner.run(suite)

    if result.failures or result.errors:
        sys.exit(1)
