# Copyright (c) 2024 - The IBus Cangjie authors
#
# This file is part of ibus-cangjie, the IBus Cangjie input method engine.
#
# ibus-cangjie is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ibus-cangjie is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ibus-cangjie.  If not, see <http://www.gnu.org/licenses/>.

import gi  # type: ignore
import unittest
from unittest.mock import create_autospec

gi.require_version("IBus", "1.0")
from gi.repository import IBus  # type: ignore

from ibus_cangjie.hotkeys import (  # type: ignore
    HotKeyManager,
    SpecialKeys,
)


class SpecialKeyTestCase(unittest.TestCase):

    def test_sort_same_type(self):
        self.assertEqual(
            sorted([SpecialKeys.SHIFT_L, SpecialKeys.SHIFT_R]),
            [SpecialKeys.SHIFT_L, SpecialKeys.SHIFT_R],
            "The order is correct and should not be change by sorting",
        )
        self.assertEqual(
            sorted([SpecialKeys.SHIFT_R, SpecialKeys.SHIFT_L]),
            [SpecialKeys.SHIFT_L, SpecialKeys.SHIFT_R],
            "Left shift key should be sorted before right shift key",
        )

    def test_sort_with_number(self):
        self.assertEqual(
            sorted([SpecialKeys.SHIFT_L, 41]),
            [41, SpecialKeys.SHIFT_L],
            "Left shfit key should be sorted after 41",
        )
        self.assertEqual(
            sorted([43, SpecialKeys.SHIFT_L]),
            [SpecialKeys.SHIFT_L, 43],
            "Left shfit key should be sorted before 43",
        )

    def test_sort_with_other_type(self):
        with self.assertRaises(TypeError):
            sorted([SpecialKeys.SHIFT_L, "foo"])
        with self.assertRaises(TypeError):
            sorted(["foo", SpecialKeys.SHIFT_L])


class HotKeyManagerTestCase(unittest.TestCase):
    def test_single_hot_key_trigger(self):
        mock_function = create_autospec(lambda a: None)
        m = HotKeyManager().register_hotkey([SpecialKeys.SHIFT_L], mock_function)
        m.do_process_key_event(0, SpecialKeys.SHIFT_L, 0)
        m.do_process_key_event(0, SpecialKeys.SHIFT_L, IBus.ModifierType.RELEASE_MASK)
        mock_function.assert_called_once_with([SpecialKeys.SHIFT_L])
        mock_function.assert_called_once()  # The function should be called only once

        # Test deregistering
        m.deregister_hotkey([SpecialKeys.SHIFT_L], mock_function)
        m.do_process_key_event(0, SpecialKeys.SHIFT_L, 0)
        m.do_process_key_event(0, SpecialKeys.SHIFT_L, IBus.ModifierType.RELEASE_MASK)
        mock_function.assert_called_once()  # The function should still be called only once

    def test_combination_hot_key_trigger(self):
        mock_function = create_autospec(lambda a: None)
        m = HotKeyManager().register_hotkey(
            [SpecialKeys.SHIFT_L, SpecialKeys.SHIFT_R], mock_function
        )
        m.do_process_key_event(0, SpecialKeys.SHIFT_L, 0)
        m.do_process_key_event(0, SpecialKeys.SHIFT_R, 0)
        m.do_process_key_event(0, SpecialKeys.SHIFT_L, IBus.ModifierType.RELEASE_MASK)
        m.do_process_key_event(0, SpecialKeys.SHIFT_R, IBus.ModifierType.RELEASE_MASK)
        mock_function.assert_called_once_with(
            [SpecialKeys.SHIFT_L, SpecialKeys.SHIFT_R]
        )
        mock_function.assert_called_once()  # The function should be called only once

        # Test deregistering
        m.deregister_hotkey([SpecialKeys.SHIFT_L, SpecialKeys.SHIFT_R], mock_function)
        m.do_process_key_event(0, SpecialKeys.SHIFT_L, 0)
        m.do_process_key_event(0, SpecialKeys.SHIFT_R, 0)
        m.do_process_key_event(0, SpecialKeys.SHIFT_L, IBus.ModifierType.RELEASE_MASK)
        m.do_process_key_event(0, SpecialKeys.SHIFT_R, IBus.ModifierType.RELEASE_MASK)
        mock_function.assert_called_once()  # The function should still be called only once

    def test_combination_hot_key_trigger_with_other_key(self):
        mock_function = create_autospec(lambda a: None)
        m = HotKeyManager().register_hotkey(
            [SpecialKeys.SHIFT_L, SpecialKeys.SHIFT_R], mock_function
        )

        m.do_process_key_event(0, SpecialKeys.SHIFT_L, 0)
        m.do_process_key_event(0, SpecialKeys.SHIFT_R, 0)
        m.do_process_key_event(
            0, 10, IBus.ModifierType.SHIFT_MASK
        )  # Pressing other keys in-between resets the combination
        m.do_process_key_event(0, SpecialKeys.SHIFT_L, IBus.ModifierType.RELEASE_MASK)
        m.do_process_key_event(0, SpecialKeys.SHIFT_R, IBus.ModifierType.RELEASE_MASK)
        mock_function.assert_not_called()
        mock_function.reset_mock()

        m.do_process_key_event(0, SpecialKeys.SHIFT_L, 0)
        m.do_process_key_event(
            0, 10, IBus.ModifierType.SHIFT_MASK
        )  # Pressing other keys in-between resets the combination
        m.do_process_key_event(0, SpecialKeys.SHIFT_R, IBus.ModifierType.RELEASE_MASK)
        m.do_process_key_event(0, SpecialKeys.SHIFT_L, IBus.ModifierType.RELEASE_MASK)
        m.do_process_key_event(0, SpecialKeys.SHIFT_R, IBus.ModifierType.RELEASE_MASK)
        mock_function.assert_not_called()
        mock_function.reset_mock()

        m.do_process_key_event(0, SpecialKeys.SHIFT_L, 0)
        m.do_process_key_event(0, SpecialKeys.SHIFT_R, 0)
        m.do_process_key_event(0, SpecialKeys.SHIFT_L, IBus.ModifierType.RELEASE_MASK)
        m.do_process_key_event(
            0, 10, IBus.ModifierType.SHIFT_MASK
        )  # Pressing other keys in-between resets the combination
        m.do_process_key_event(0, SpecialKeys.SHIFT_R, IBus.ModifierType.RELEASE_MASK)
        mock_function.assert_not_called()
