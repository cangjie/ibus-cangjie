# Unit Tests

This folder is a valid and runnable Python module. You may run the tests here
in the __parent folder__.


## Run the Tests

To run the tests:

```bash
python -m tests
```

## Coverage Report

If you have the [coverge][python-coverage] package installed, you can also run
generate and review the coverage reports:

```bash
coverage run -m tests
coverage report
```

Or for some reason, coverage is not installed to your `PATH`:

```bash
python -m coverage run -m tests
python -m coverage report
```

Note: the above command will leave a `.coverage` file in the parent folder.


## Colored Test Output (Optional)

To get more readable test output, you may install the [colour-runner][colour-runner]
before running tests:

```bash
pip install colour-runner
```


[python-coverage]: https://pypi.org/project/coverage/
[colour-runner]: https://github.com/meshy/colour-runner/
