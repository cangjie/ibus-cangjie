# Copyright (c) 2013 - The IBus Cangjie authors
#
# This file is part of ibus-cangjie, the IBus Cangjie input method engine.
#
# ibus-cangjie is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ibus-cangjie is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ibus-cangjie.  If not, see <http://www.gnu.org/licenses/>.


import gi  # type: ignore
import os
import sys
import unittest


def has_gtk(version):
    """Detect if we have Gtk installed

    It is very common to run the unit tests in an environment which does not
    have any graphical capabilities.

    This is for example the case in a CI server, or when building RPMs for
    Fedora.

    This function is useful to detect these situation, so that we can
    automatically skip the tests which can't run without.
    """
    try:
        gi.require_version("Gtk", version)
        from gi.repository import Gtk  # type: ignore

        Gtk.init()

    except ValueError as e:
        print(f"Gtk not available: {e}", file=sys.stderr)
        return

    except RuntimeError as e:
        # On some platforms (e.g Ubuntu 12.04 where our CI is running) we
        # can't import Gtk without a display.
        #
        # We also depends on libadwaita, which might not be available on all
        # platforms.
        print(f"Gtk not available: {e}", file=sys.stderr)
        return False

    return True


def has_libadwaita(version):
    """Detect if we have graphical library installed.

    Libawaita is used to theme the UI element (the about dialog, to be
    specific).

    This function is useful to detect these situation, so that we can
    automatically skip the tests which can't run without.
    """
    try:
        import gi

        gi.require_version("Adw", version)
        from gi.repository import Adw  # type: ignore

        Adw.init()

    except ValueError as e:
        print(f"Adw not available: {e}", file=sys.stderr)
        return

    except RuntimeError as e:
        # On some platforms (e.g Ubuntu 12.04 where our CI is running) we
        # can't import Gtk without a display.
        #
        # We also depends on libadwaita, which might not be available on all
        # platforms.
        print(f"Adw not available: {e}", file=sys.stderr)
        return False

    return True


def has_gdk_default_display(version):
    """Detect if we have graphical capabilities.

    Evan if Gtk is available, we might not have a display. And the lack of
    a display sometimes can cause the tests to fail.

    This function is useful to detect these situation, so that we can
    automatically skip the tests which can't run without.
    """
    try:
        # Note: some platforms (e.g Fedora 21) can import Gtk just fine even
        # without a display...
        gi.require_version("Gdk", version)
        from gi.repository import Gdk  # type: ignore

        if Gdk.Display.get_default() is None:
            # ... We don't have a display
            print("Gdk unable to get default display:", file=sys.stderr)
            return False

    except ValueError as e:
        print(f"Gdk not available: {e}", file=sys.stderr)
        return

    except RuntimeError as e:
        # On some platforms (e.g Ubuntu 12.04 where our CI is running) we
        # can't import Gtk without a display.
        #
        # We also depends on libadwaita, which might not be available on all
        # platforms.
        print(f"Gdk not available: {e}", file=sys.stderr)
        return False

    return True


class PrefsTestCase(unittest.TestCase):
    def setUp(self):
        self.ui_file = os.path.join(os.environ["PREFERENCES_UI_DIR"], "setup.ui")

    def tearDown(self):
        pass

    def test_ui_file_is_valid_xml(self):
        from xml.etree import ElementTree as ET

        try:
            ET.parse(self.ui_file)

        except ET.ParseError as e:
            raise AssertionError(e)

    @unittest.skipUnless(has_gtk("4.0"), "Can't use GtkBuilder")
    @unittest.skipUnless(
        has_gdk_default_display("4.0"), "Can't get default display with Gdk"
    )
    @unittest.skipUnless(has_libadwaita("1"), "Can't use Libadwaita")
    def test_ui_file_is_valid_gtk_builder(self):
        from gi.repository import Gtk

        b = Gtk.Builder()

        try:
            b.add_from_file(self.ui_file)

        except Exception as e:
            raise AssertionError(e)
